// Data channel information

let addressConnect = `${document.location.hostname}:8282`,
    dataChannel,
    myName = "You",
    partnerName = "Partner",

    // Flags...
    rejected = false,
    desktopNotification = false,
    TypeWriter = false,
    socket,
    pc_config = {
        'iceServers': [ {
                'urls': 'stun:stun.l.google.com:19302'
            }]
    },

    // HTML5 elements
    sendButton = document.querySelector("#sendButton"),
    chatContainer = document.querySelector("#chatContainer"),
    dataInput = document.querySelector("#dataInput"),
    stanza = document.querySelector("#stanza"),
    answerButton = document.querySelector("#answerBtn"),
    declineButton = document.querySelector("#declineBtn"),
    buttons = document.querySelector("#answerButtons"),
    typeWriterTD = document.querySelector("#typeWriterTD"),
    pc_constraints = {
        'optional': [{
            "DtlsSrtpKeyAgreement": true
        }]
    },
    userRoom = null,
    pc;


const urlParams = new URLSearchParams(location.search);
const headerRoom = urlParams.get('room');

// Handler associated with 'Send' button
if (sendButton) sendButton.onclick = function () {
    handleInputField(dataInput.value);
};

pc = new RTCPeerConnection(pc_config);

// Let's get started: prompt user for input (room name)
// Connect to signalling server
console.log("Connecting to the server...");

try {
    socket = io(addressConnect, {
        secure: true
    });
} catch (error) {
    console.warn("Error connecting to server:\n", error);
}


if (headerRoom) {
    userRoom = headerRoom.toString();
} else {
    userRoom = prompt('Enter room name (if you press "enter" a random room will be generated).');
    if (userRoom === '') {
        userRoom = String(Math.floor((Math.random() * 10000) + 1));
        window.history.pushState("object or string", "Title", `/?room=${userRoom}`);

    }
}

console.log(`Asking for the room '${userRoom}'...`);
socket.emit('create or join', userRoom);
document.querySelector('#title').innerHTML = userRoom + ' - WebRTC Terminal Chat.';

// S O C K E T . I O ==================================
socket.on('created', function (userRoom) {
    console.log(`This peer is the the first in the room. Socket id: ${socket.id}`);
    stanza.innerHTML = `You are in room '${userRoom}'`;
});

socket.on('joined', function (userRoom) {
    console.debug(`DEBUG: This peer has joined room '${userRoom}'.`);
    stanza.innerHTML = `You have joined to room '${userRoom}'. Please wait for the other user to answer.`;

    sendMessage({
        message: 'call',
        room: userRoom
    });
});

// Handle 'full' message coming back from server: this peer arrived too late :-(
socket.on('full', function () {
    alert(`I am sorry, this room is already occupied!`);
    rejected = true;
});

// Server-sent log message...
socket.on('log', function (array) {
    console.log.apply(console, array);
});

socket.on('message', function (message) {
    console.log("Received message:", message);

    switch (true) {
        case (message.msg.message === 'refused' && message.room === userRoom) :
            printMessageInfo("isTypingTD", "Call refused.", 0);
            break;

        case (message.msg.message === 'accepted' && message.room === userRoom) :
            // 1* step.
            createPeerConnection(pc_config, pc_constraints);
            break;

        case (message.msg.message === 'call' && message.room === userRoom) :
            buttons.hidden = false;
            answerButton.addEventListener("click", function () {

                createPeerConnection(pc_config, pc_constraints);
                pc.createOffer(setLocalAndSendMessage, onSignalingError);
                sendMessage({
                    message: 'accepted',
                    room: userRoom
                });
                buttons.hidden = true;
            });

            declineButton.addEventListener("click", function () {
                sendMessage({
                    message: 'refused',
                    room: userRoom
                });
                buttons.hidden = true;
            });
            break;

        case (message.msg.type === 'offer') :
            pc.setRemoteDescription(new RTCSessionDescription(message.msg));
            pc.createAnswer(setLocalAndSendMessage, onSignalingError);
            break;

        case (message.msg.type === 'answer') :
            pc.setRemoteDescription(new RTCSessionDescription(message.msg));
            break;

        case (message.msg.type === 'candidate') :

            let candidate = new RTCIceCandidate({
                sdpMLineIndex: message.msg.label,
                candidate: message.msg.candidate
            });
            pc.addIceCandidate(candidate);
            break;

        case (message.msg.message === 'bye' && message.room === userRoom) :
            sendMessage({
                message: 'ok_bye',
                room: userRoom
            });
            printMessageInfo("isTypingTD", "User disconnected.", 0);
            break;

        case (message.msg.message === 'ok_bye' && message.room === userRoom) :
            console.log("ok, bye!");
            printMessageInfo("isTypingTD", "User disconnected.", 0);
            break;

        default:
            console.error("Message incorrect:", message);
            break;
    }


});

//=============================================================

function sendMessage(message) {

    let msg = {
        msg: message,
        room: userRoom
    };

    console.log('➢ : ', message);
    socket.emit('message', msg);
}

function errorOnLocalMedia(err) {
    console.log(err.name + ": " + err.message);
}

// PeerConnection management.==================================
function createPeerConnection(config, constraints) {
    try {
        pc = new RTCPeerConnection(config, constraints);
        pc.onicecandidate = (event) => {
            if (event.candidate) {
                sendMessage({
                    type: 'candidate',
                    label: event.candidate.sdpMLineIndex,
                    id: event.candidate.sdpMid,
                    candidate: event.candidate.candidate
                });
            } else {
                console.log('DEBUG: End of candidates.');
            }
        };

        try {
            dataChannel = pc.createDataChannel("DataChannel", {
                ordered: true
            });
            console.log('Created send data channel');

            dataChannel.onopen = handleChannelStateChange;
            dataChannel.onmessage = printMessage;
            dataChannel.onclose = handleChannelStateChange;
            console.log("Event from remote Peer Connection on data channel.");
            pc.ondatachannel = (event) => {
                dataChannel = event.channel;
                dataChannel.onmessage = printMessage;
                dataChannel.onopen = handleChannelStateChange;
                dataChannel.onclose = handleChannelStateChange;
            };

        } catch (e) {
            alert('Failed to create data channel: ', e.message);

            console.log('createDataChannel() failed with exception: ' + e.message);
        }

    } catch (e) {
        console.log('DEBUG: Failed to create PeerConnection, exception: ' + e.message);
        alert('Cannot create RTCPeerConnection object.');
    }


}

// H a n d l e r s ============================================


// Data channel management
function printMessage(event) {
    data = JSON.parse(event.data);
    console.log('Received message on RTC channel:\n', data);

    switch (true) {
        case (data.type === "ping"):
            msg = {
                type: "pong",
                time: data.serial
            };
            dataChannel.send(JSON.stringify(msg));
            break;

        case (data.type === "pong"):
            break;

        case (data.type === "mynameis"):
            partnerName = data.name;
            chatContainer.innerText += `User changed its name with '${data.name}'\n`;
            window.scrollTo(0, document.body.scrollHeight);
            break;

        case (data.type === "itsnameis"):
            myName = data.name;
            chatContainer.innerText += `User changed your name with '${data.name}'\n`;
            document.getElementById("myName").innerText = `@${myName} ~ $:`;
            window.scrollTo(0, document.body.scrollHeight);
            break;

        case (data.type === "isTyping"):
            printMessageInfo("isTypingTD", "is typing...", 3000);
            break;

        case (data.type === "typewriting"):
            typeWriterTD.innerText = `TypeWriter: ${data.message}`;
            break;

        case (data.type === "typewriter"):
            TypeWriter = data.value;
            if (TypeWriter) {
                chatContainer.innerText += `${getTime(new Date())} - ${partnerName} enabled TypeWriter.\n`;
                typeWriterTD.hidden = false;
            } else {
                chatContainer.innerText += `${getTime(new Date())} - ${partnerName} disabled TypeWriter.\n`;
                typeWriterTD.hidden = true;
            }
            break;

        default:
            chatContainer.innerText += `${getTime(new Date())} - ${partnerName}: ${data.message}`;
            window.scrollTo(0, document.body.scrollHeight);
            typeWriterTD.innerText = "";
            if (desktopNotification) windowNotification(userRoom, partnerName, data.message);
            break;
    }
}

function handleInputField(data) {
    let dataSplit = data.split(/[ \n]+/);
    let msg = {};

    function scrollAndClean() {
        dataInput.value = "";
        window.scrollTo(0, document.body.scrollHeight);
    }

    switch (true) {
        case (dataSplit[0] === "--mynameis" || dataSplit[0] === "—-mynameis"):
            myName = dataSplit[1];
            chatContainer.innerText += `You have changed your name with '${myName}'\n`;
            document.getElementById("myName").innerText = `@${myName} ~ $:`;
            msg = {
                type: "mynameis",
                name: myName
            };
            dataChannel.send(JSON.stringify(msg));
            break;

        case (dataSplit[0] === "--itsnameis" || dataSplit[0] === "—-itsnameis"):
            partnerName = dataSplit[1];
            chatContainer.innerText += `You have changed your partner's name with '${partnerName}'\n`;
            msg = {
                type: "itsnameis",
                name: partnerName
            };
            dataChannel.send(JSON.stringify(msg));
            break;

        case (dataSplit[0] === "--headerColor" || dataSplit[0] === "—-headerColor"):
            document.getElementById("stanza").style.backgroundColor = dataSplit[1];
            break;

        case (dataSplit[0] === "--help" || dataSplit[0] === "—-help"):
            help();
            break;

        case (dataSplit[0] === "--clear" || dataSplit[0] === "—-clear"):
            chatContainer.innerText = `\n`;
            break;

        case (dataSplit[0] === "--TypeWriter" || dataSplit[0] === "—-TypeWriter"):
            if (dataSplit[1] === "on") {
                msg = {
                    type: "typewriter",
                    value: true
                };
                TypeWriter = true;
                dataChannel.send(JSON.stringify(msg));
                chatContainer.innerText += `${getTime(new Date())} - You have enabled TypeWriter. Now ${partnerName} will see what you type in real time\n`;

            }
            else if (dataSplit[1] === "off"){
                msg = {
                    type: "typewriter",
                    value: false
                };
                TypeWriter = true;
                dataChannel.send(JSON.stringify(msg));
                chatContainer.innerText += `${getTime(new Date())} - You have disabled TypeWriter.\n`;

            } else {
                chatContainer.innerText += `Invalid TypeWriter set. It can be 'TypeWriter on' or 'TypeWriter off'.\n`;
            }
            break;

        case (dataSplit[0] === "--notifyMe"):
            if (dataSplit[1] === "on") {

                if (!("Notification" in window)) {
                    console.warn("This browser does not support desktop notification");
                    chatContainer.innerText += `Warning: Your browser does not support desktop notification.'\n`;
                    desktopNotification = false;
                }

                // Otherwise, we need to ask the user for permission
                else if (Notification.permission !== "denied") {
                    Notification.requestPermission().then(function (permission) {
                        desktopNotification = true;
                        chatContainer.innerText += `You have turned on desktop notifications.\n`;
                    });
                } else if (Notification.permission === "denied") {
                    Notification.requestPermission().then(function (permission) {
                        desktopNotification = false;
                        chatContainer.innerText += `You have previously blocked notification for this website.\n`;
                    });
                } else {
                    desktopNotification = false;
                    chatContainer.innerText += `Error: we encountered a generic error while trying to apply notification\n`;
                }


            } else if (dataSplit[1] === "off") {
                desktopNotification = false;
                chatContainer.innerText += `You have turned off desktop notifications.\n`;

            } else {
                chatContainer.innerText += `Invalid notification set. It can be 'notifyMe on' or 'notifyMe off'.\n`;
            }
            break;

        default:
            msg = {
                type: "message",
                message: data
            };
            dataChannel.send(JSON.stringify(msg));
            chatContainer.innerText += `${getTime(new Date())} - ${myName}: ${msg.message}`;
            console.log('Sent data: ', msg);
            break;
    }

    scrollAndClean();

}

function handleChannelStateChange() {
    try {
        console.log('data channel state is: ' + dataChannel.readyState);
    } catch (e) {
        console.log("Error on data channel:\n", e);
    }

    /* If channel ready, enable user's input*/
    if (dataChannel.readyState === 'open') {
        dataInput.disabled = false;
        chatContainer.innerText = `Both are connected to the room.\n`;
        chatContainer.innerText += `Type '--help' to find out what you cand do beyond chat!'\n`;

        document.addEventListener("keyup", event => {
            dataChannel.send(JSON.stringify({type: 'isTyping'}));
            if (TypeWriter) {
                let msg = {
                    type: "typewriting",
                    message: dataInput.value
                };
                dataChannel.send(JSON.stringify(msg));
            }
        });


        dataInput.focus();
        sendButton.disabled = false;
        stanza.innerHTML = `Connected to the room '${userRoom}'.`;
        sendPing();
    } else {
        dataInput.disabled = true;
        sendButton.disabled = true;
    }
}

// Signalling error handler
function onSignalingError(error) {
    console.log('DEBUG: Failed to create signaling message : ' + error.name);
}


// Success handler for both createOffer() and createAnswer()
function setLocalAndSendMessage(sessionDescription) {
    pc.setLocalDescription(sessionDescription);
    sendMessage(sessionDescription);
}


function sendPing() {
    thisTime = new Date().getTime();
    msg = {
        type: "ping",
        serial: thisTime

    };
    let interval = Math.floor(Math.random() * 50000) + 1;
    console.log(`Next ping #${thisTime} in ${interval / 1000} secs.`);
    dataChannel.send(JSON.stringify(msg));
    setTimeout(sendPing, interval);
}

function windowNotification(room, user, message) {
    let notification = new Notification(`[${room}] ${user}: ${message}`);
}

function hangup() {
    if (!rejected) {
        sendMessage({
            message: 'bye',
            room: userRoom
        });

        rejected = false;
        if (dataChannel) dataChannel.close();
        if (pc) pc.close();
        pc = null;
        console.log("Hangup: everything is done!");
    }
}

/**
 *
 * @param DivId String of the div Id where to place this message
 * @param message String of the message
 * @param cleanDelay Integer of delay to clean the message in milliseconds.
 */
function printMessageInfo(DivId, message, cleanDelay) {

    let elem = document.getElementById(DivId);

    if (elem.innerText !== message) {
        elem.innerText = message;
        if (cleanDelay > 0 ) {
            setTimeout(function () {
                document.getElementById(DivId).innerHTML = "&nbsp;";
            }, cleanDelay);
        }
    }

}

// Clean-up function:
// collect garbage before unloading browser's window
window.onbeforeunload = function (e) {
    console.log("On before upload..." + e);
    if (!rejected) hangup();
};

function enterKeyPress(e) {
    if (typeof e === 'undefined' && window.KeyboardEvent) {
        e = window.KeyboardEvent;
    }
    if (e.keyCode === 13) {
        document.getElementById('sendButton').click();
    }
}

function getTime(date) {
    let hours = date.getHours(),
        minutes = date.getMinutes(),
        ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    return hours + ':' + minutes + ' ' + ampm;
}

function help() {
    chatContainer.innerText += `\n`;
    chatContainer.innerText += `--mynameis [your name]         Changes your name in the chat.\n`;
    chatContainer.innerText += `--itsnameis [partner's name]   Changes your partner's name in the chat.\n`;
    chatContainer.innerText += `--notifyMe [on/off]            Turns on/off the desktop notification (it could not work on mobile).\n`;
    chatContainer.innerText += `--TypeWriter [on/off]          Turns on/off the real time message field (like TypeWriter :) ).\n`;
    chatContainer.innerText += `--headerColor [color name]     Changes the header color. For a complete list of color names please visit 'https://www.w3schools.com/colors/colors_names.asp.\n\n`;
    chatContainer.innerText += `--clear                        Clears the chat.\n`;
    chatContainer.innerText += `--help                         Help Menu.'\n\n`;
}



