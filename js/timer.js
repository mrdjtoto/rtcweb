'use strict';

function Timer(divPosition) {
    var hour = 0, minute = 0, second = 0;
    var ora = second + "s. ";
    divPosition.innerText = ora;
    var timing;
    var isStarted = false;


    Timer.prototype.start = function () {
        if (!isStarted) {
            console.log("Called function start");
            timing = setInterval(function () {
                timer.ahead()
            }, 1000);
            console.log("Called function ahead");
            isStarted = true;
        }
    }


    Timer.prototype.stop = function () {
        if (isStarted) {
            console.log("Called function stop");
            clearInterval(timing);
            isStarted = false;
        }
    }


    Timer.prototype.ahead = function () {
        ++second;
        if (second === 60) {
            if (minute === 59) {
                hour++
                minute = 0;
                second = 0;
            } else {
                minute++
                second = 0;
            }
        }
        if (hour) {
            ora = hour + "h " + minute + "m " + second + "s. ";
        } else if (minute) {
            ora = minute + "m " + second + "s. ";
        } else {
            ora = second + "s. ";
        }

        divPosition.innerText = ora;
    };


    Timer.prototype.reset = function () {
        console.log("Called function reset");
        hour = 0, minute = 0, second = 0;
        ora = second + "s. ";
        divPosition.innerText = ora;
        if (isStarted) {
            this.stop();
            this.start();
        }
    }
}