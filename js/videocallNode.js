'use strict';

//navigator contiene le info sul browser
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

// Clean-up function:
// collect garbage before unloading browser's window
window.onbeforeunload = function (e) {
    hangup();
}

// Data channel information
//var addressConnect = "http://localhost:8181"
var addressConnect = "http://rtcweb.ddns.net"

var stanza = document.getElementById("stanza");

// HTML5 <video> elements
var localVideo = document.querySelector('#localVideo');
var remoteVideo = document.querySelector('#remoteVideo');

// Handler associated with 'Send' button

// Flags...
var isChannelReady = false;
var isInitiator = false;
var isStarted = false;
var isObtained = false;
var terzoIncomodo = false;
var isCreated = false;

// WebRTC data structures Streams
var localStream;
var remoteStream;

// Peer Connection
var pc;

// Peer Connection ICE protocol configuration (either Firefox or Chrome)
var pc_config = webrtcDetectedBrowser === 'firefox' ?
    {'iceServers': [{'url': 'stun:23.21.150.121'}]} : // IP address
    {'iceServers': [{'url': 'stun:stun.l.google.com:19302'}]};

var pc_constraints = {'optional': [{'DtlsSrtpKeyAgreement': true}]};

// Session Description Protocol constraints:
var sdpConstraints = {};
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

// Let's get started: prompt user for input (room name)
var room = prompt('Enter room name (if you press "enter" a random room will be generated).');

// Connect to signalling server
var socket = io.connect(addressConnect);

// Send 'Create or join' message to singnalling server
if (room !== '') {
    socket.emit('create or join', room);
    isCreated = true;
} else {
    room = Math.floor((Math.random() * 10000) + 1);
    socket.emit('create or join', room);
}

// Set getUserMedia constraints

var constraints = {video: {mandatory: {maxWidth: 360, maxHeight: 270}}, audio: true};

// From this point on, execution proceeds based on asynchronous events...

/////////////////////////////////////////////

// getUserMedia() handlers...
/////////////////////////////////////////////
function handleUserMedia(stream) {
    localStream = stream;
    if (window.URL) {
        localVideo.src = window.URL.createObjectURL(stream);
    } else {
        localVideo.src = stream;
    }
    sendMessage('got user media');
    isObtained = true;
    isChannelReady = true;
}

function handleUserMediaError(error) {
    console.log('navigator.getUserMedia error: ', error);
}

/////////////////////////////////////////////


// Server-mediated message exchanging...
/////////////////////////////////////////////

// 1. Server-->Client...
/////////////////////////////////////////////

// Handle 'created' message coming back from server:
// this peer is the initiator
socket.on('created', function (room) {
        isInitiator = true;
        navigator.getUserMedia(constraints, handleUserMedia, handleUserMediaError);
        if (isCreated) {
            var texto = document.createTextNode('You have created room: ' + room);
            stanza.appendChild(texto);
        } else {
            var texto = document.createTextNode('Auto-generated room: ' + room);
            stanza.appendChild(texto);
        }
        //checkAndStart(); <-- qui non lo eseguirà mai
    }
);

// Handle 'join' message coming back from server:
// another peer is joining the channel
socket.on('join', function (room) {
        //var answer = confirm ("New incoming call. Answer?")
        if (confirm("New incoming call. Answer?") && isObtained) {
            sendMessage('got user media');
        } else sendMessage('refused');
    }
);

socket.on('joined', function (room) {
        var texto = document.createTextNode('You are joined to room: ' + room + '. Please wait for the answer...');
        stanza.appendChild(texto);
    }
);


// Handle 'full' message coming back from server:
// this peer arrived too late :-(
socket.on('full', function (room) {
        alert('I am sorry, this room is already occupied!');
        terzoIncomodo = true;
    }
);

// Server-sent log message...
socket.on('log', function (array) {
    console.log.apply(console, array);
});

// Receive message from the other peer via the signalling server 
socket.on('message', function (message) {
        if (message === 'refused') {
            alert("Call refused!");
            hangup();
        } else if (message === 'got user media') {
            if (!isInitiator) {
                navigator.getUserMedia(constraints, handleUserMedia, handleUserMediaError);
            }
            checkAndStart();
        } else if (message.type === 'offer') {
            if (!isInitiator && !isStarted) {
                checkAndStart();
            }
            pc.setRemoteDescription(new RTCSessionDescription(message));
            doAnswer();
        } else if (message.type === 'answer' && isStarted) {
            pc.setRemoteDescription(new RTCSessionDescription(message));
        } else if (message.type === 'candidate' && isStarted) {
            var candidate = new RTCIceCandidate({sdpMLineIndex: message.label, candidate: message.candidate});
            pc.addIceCandidate(candidate);
        } else if (message === 'bye' && isStarted) {
            handleRemoteHangup();
        } else if (message === 'typing' && isStarted) {
            handleIsTyping();
        }
    }
);
////////////////////////////////////////////////

// 2. Client-->Server
////////////////////////////////////////////////
// Send message to the other peer via the signalling server
function sendMessage(message) {
    socket.emit('message', message);
}

////////////////////////////////////////////////////

////////////////////////////////////////////////////
// Channel negotiation trigger function
function checkAndStart() {
    //if (!isStarted && typeof localStream != 'undefined' && isChannelReady) {
    if (!isStarted && typeof localStream != 'undefined' && isChannelReady && isObtained) {
        createPeerConnection();
        isStarted = true;
        if (isInitiator) {
            doCall();
        }
    }
}

/////////////////////////////////////////////////////////
// Peer Connection management...
function createPeerConnection() {
    try {
        pc = new RTCPeerConnection(pc_config, pc_constraints);
        pc.addStream(localStream);
        pc.onicecandidate = handleIceCandidate;

    } catch (e) {
        alert('Cannot create RTCPeerConnection object.');
        return;
    }

    pc.onaddstream = handleRemoteStreamAdded;
    pc.onremovestream = handleRemoteStreamRemoved;
}


// ICE candidates management
function handleIceCandidate(event) {
    if (event.candidate) {
        sendMessage({
            type: 'candidate',
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate
        });
    } else {
        console.log('End of candidates.');
    }
}

// Create Offer
function doCall() {
    pc.createOffer(setLocalAndSendMessage, onSignalingError, sdpConstraints);
}

// Signalling error handler
function onSignalingError(error) {
    console.log('Failed to create signaling message : ' + error.name);
}

// Create Answer
function doAnswer() {
    pc.createAnswer(setLocalAndSendMessage, onSignalingError, sdpConstraints);
}

function isTyping() {
    sendMessage('typing');
}

function handleIsTyping() {
    var output = document.getElementById("risultato");
    output.innerHTML = "";
    if (txt != 'Sta scrivendo...') {
        var txt = document.createTextNode("is typing...");
        output.appendChild(txt);
        setInterval(function () {
            txt.parentNode.removeChild(txt);
        }, 1000);
    }
}


// Success handler for both createOffer()
// and createAnswer()
function setLocalAndSendMessage(sessionDescription) {
    pc.setLocalDescription(sessionDescription);
    sendMessage(sessionDescription);
}

/////////////////////////////////////////////////////////
// Remote stream handlers...

function handleRemoteStreamAdded(event) {
    attachMediaStream(remoteVideo, event.stream);
    remoteStream = event.stream;
}

function handleRemoteStreamRemoved(event) {
    console.log('Remote stream removed. Event: ', event);
}

/////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////
// Clean-up functions...

function hangup() {
    if (!terzoIncomodo) {
        stop();
        sendMessage('bye');
        isChannelReady = false;
    }
}

function handleRemoteHangup() {
    if (!terzoIncomodo) {
        stop();
    }
}

function stop() {
    if (!terzoIncomodo) {
        isStarted = false;

        if (pc) pc.close();
        pc = null;
        sendButton.disabled = true;
    }
}
