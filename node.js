const fs = require('fs'),
    https = require('spdy'),
    express = require('express'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    Winston = require('winston'),
    app = express();

let config = JSON.parse(fs.readFileSync("config.json"));


const options = {
    httpServer: {
        cert: fs.readFileSync(config.httpServer.cert).toString(),
        key: fs.readFileSync(config.httpServer.key).toString(),
        //ca: fs.readFileSync("certificates/rtcweb_it_apache.crt"),
        port: config.httpServer.port,
        host: config.httpServer.host
    },
    winston: {
        level: config.winston.level,

        /*
        Winston debug levels:
          error: 0,
          warn: 1,
          info: 2,
          verbose: 3,
          debug: 4,
          silly: 5
         */
        transports: [
            new Winston.transports.Console({
                timestamp: true
            })
        ]
    }
};

const logger = new Winston.Logger(options.winston);

let server = https.createServer(options.httpServer, app).listen(options.httpServer.port, function () {
        logger.info(`Express connected. Server active at ${options.httpServer.host}:${options.httpServer.port}`);
        logger.info(`Server started in ${options.winston.level} mode.`)
    }),
    io = require('socket.io').listen(server);
app.use(cookieParser());
app.use(session(config.express_session));

app.get('/*', function (req, res) {
    let clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    let url = require('url');
    let url_parts = url.parse(req.url, true);
    let queryRoom = url_parts.query.room;

    if (queryRoom) logger.debug(`${clientIp} - SId:${req.sessionID} [GET] - ${req.path} - Query:${queryRoom}`);
    else logger.debug(`${clientIp} - SId:${req.sessionID} [GET] - ${req.path}`);

    if (req.url === "/activerooms.json") {
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json(io.sockets.adapter.rooms);

    } else {
        try {
            res.sendFile(__dirname + url.parse(req.url).pathname);

        } catch (err) {
            logger.error(`Request ${req.url} can't be handled due of an error:\b${err.stack}`);
        }
    }
});


// Let's start managing connections...
io.on('connection', function (socket) {

    // Handle 'message' messages
    socket.on('message', function (message) {

        message.timestamp = new Date().getTime();
        logger.info(`got message from room '${message.room}':\n${JSON.stringify(message, null, 2)}`);

        // channel-only broadcast...
        socket.to(message.room).emit('message', message); //broadcast a tutti tranne chi ha inviato il messaggio
        logger.info(`Message ${message.timestamp} broadcasted to room '${message.room}'.`);
    });

    // Handle 'create or join' messages
    socket.on('create or join', function (room) {

        if (io.sockets.adapter.rooms[room])
            logger.debug(`Called 'create or join' message for room ${room} => ${JSON.stringify(io.sockets.adapter.rooms[room])}:${io.sockets.adapter.rooms[room].length}`);
        // First client joining...
        if (!io.sockets.adapter.rooms[room]) {
            socket.join(room); //join alla room stabilita
            socket.emit('created', room);
            logger.info('Session id ' + socket.id + ' - Created room ' + room + '.');

        } else if (io.sockets.adapter.rooms[room].length === 1) {
            // Second client joining...
            io.in(room).emit('join', room); //broadcast a tutti
            socket.join(room);
            socket.emit('joined', room); //messaggio di signaling: emette un evento a tutti i client
            logger.info('New Join in room ' + room + '.');

        } else { // max two clients
            socket.emit('full', room);
            logger.info('Room ' + room + ' is full.');
        }

        logger.debug(`Room ${room} has ${io.sockets.adapter.rooms[room].length} client(s)`);
        logger.debug(`Now active rooms are:\n${JSON.stringify(io.sockets.adapter.rooms, null, 2)}`);

    });
});